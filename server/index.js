const express = require("express");
const app = express();
const cors = require("cors");
const pool = require("./db.js");
const bodyParser = require("body-parser");

app.use(express.json());
app.use(cors());

app.post("/infos", async (req, res) => {
  try {
    const description = req.body.description;
    console.log("req.body", req.body.description);

    const todo = await pool.query(
      "INSERT INTO info (description) VALUES($1) RETURNING *",
      [description]
    );
    res.json(todo.rows[0]);
  } catch (error) {
    console.error(error.message);
  }
});
app.get("/infos", async (req, res) => {
  try {
    const getTodo = await pool.query("Select * from info");
    res.json(getTodo.rows);
  } catch (error) {
    console.error(error.message);
  }
});

app.get("/infos/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const todo = await pool.query("Select * from info where infoid=$1", [id]);
    res.json(todo.rows);
  } catch (error) {
    console.error(error.message);
  }
});

app.put("/infos/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const { description } = req.body;

    const todo = await pool.query(
      "update info set description=$1 where infoid=$2",
      [description, id]
    );
    res.json(todo);
  } catch (error) {
    console.error(error.message);
  }
});

app.delete("/infos/:id", async (req, res) => {
  try {
    const { id } = req.params;
    console.log(req.params.id);
    const todo = await pool.query("delete from info where infoid=$1", [id]);
    res.json(todo);
  } catch (error) {
    console.error(error.message);
  }
});

app.listen(5000, () => {
  console.log("listening to poprt 5000");
});
