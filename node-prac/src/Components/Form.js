import React from "react";
import { StyleSheet, css } from "aphrodite/no-important";
import axios from "axios";

const s = StyleSheet.create({
  formDiv: {
    display: "flex",
    flexDirection: "column",
  },
  firstDiv: {},
  lastDiv: {
    marginBottom: "1em",
  },
  submitDiv: {},
});

class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { description: "111", id: "222" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event, val) {
    if (val === "firstname") {
      this.setState({ description: event.target.value });
    } else {
      this.setState({ id: event.target.value });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    // try {
    //   console.log("state", this.state);
    //   const descrip = this.state;

    //   console.log(descrip);
    //   fetch("http://localhost:5000/infos", {
    //     method: "POST",

    //     mode: "cors",
    //     headers: {
    //       "Content-Type": "application/json",
    //     },
    //     body: JSON.stringify(descrip),
    //   }).then((response) => {
    //     console.log("res....", response);
    //   });
    // } catch (error) {
    //   console.error(error.message);
    // }

    // try {
    //   console.log("state", this.state);
    //   const descrip = this.state.id;

    //   console.log(descrip);
    //   fetch("http://localhost:5000/infos/" + descrip, {
    //     method: "DELETE",

    //     mode: "cors",
    //     headers: {
    //       "Content-Type": "application/json",
    //     },
    //   }).then((response) => {
    //     console.log("res....", response);
    //   });
    // } catch (error) {
    //   console.error(error.message);
    // }

    try {
      console.log("state", this.state);
      const descrip = this.state;

      console.log(descrip);
      fetch("http://localhost:5000/infos/" + this.state.id, {
        method: "PUT",
        mode: "cors",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(descrip),
      }).then((response) => {
        console.log("res....", response);
      });
    } catch (error) {
      console.error(error.message);
    }
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className={css(s.formDiv)}>
          <div className={css(s.firstDiv)}>
            {" "}
            <label>
              FirstName:
              <input
                type="text"
                value={this.state.description}
                onChange={(e) => this.handleChange(e, "firstname")}
              />
            </label>
          </div>
          <div className={css(s.lastDiv)}>
            {" "}
            <label>LastName:</label>
            <input
              type="text"
              value={this.state.id}
              onChange={(e) => this.handleChange(e, "lastname")}
            />
          </div>
        </div>
        <div className={css(s.submitDiv)}>
          <input type="submit" value="Submit" />
        </div>
      </form>
    );
  }
}

export default NameForm;
